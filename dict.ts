#!/usr/bin/env node

import dotenv from "dotenv";

dotenv.config();

import commander from "commander";
import WordPlay from "./src/handlers/WordPlay";
import {header, log} from "./src/utils/logger";
import Examples from "./src/handlers/Examples";
import FullDict from "./src/handlers/FullDict";
import Definitions from "./src/handlers/Definitions";
import RelatedWords from "./src/handlers/RelatedWords";
import WordDayFullDict from "./src/handlers/WordDayFullDict";

(async function () {

    log(header("===================================="));
    log(header("Command Line Dictionary Tool"));
    log(header("===================================="));
    log("\n");

    if (process.argv.length === 2) {
        await WordDayFullDict.runRandomWordFullDictAction();
    }
})();

commander.version("1.0.0");

commander
    .command("defn <word>")
    .description("Display definitions of a given word.")
    .action(Definitions.runWordDefinitionAction);

commander
    .command("syn <word>")
    .description("Display synonyms of a given word.")
    .action(RelatedWords.runWordSynonymsAction);

commander
    .command("ant <word>")
    .description(
        "Display antonyms of a given word. Example words with antonyms: single, break, start."
    )
    .action(RelatedWords.runWordAntonymsAction);

commander
    .command("ex <word>")
    .description("Display examples of usage of a given word in a sentence.")
    .action(Examples.runWordExamplesAction);

commander
    .command("play")
    .description("The command should display a definition, a synonym or an antonym and ask the user to guess the word.")
    .action(WordPlay.runWordPlayAction);

commander
    .arguments("<word>")
    .description("Display Word Definitions, Word RelatedWords, Word Antonyms & Word Examples for a given word.")
    .action(FullDict.runFullDictAction);

commander.parse(process.argv);