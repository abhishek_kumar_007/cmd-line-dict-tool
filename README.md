Command Line Dictionary Tool
===============================

The command-line tool to fetch word definitions, synonyms, antonyms, examples. 

Libraries
---------

* [chalk](https://www.npmjs.com/package/chalk) colorizes the output
* [inquirer](https://www.npmjs.com/package/inquirer)  creates interactive command-line user interface
* [axios](https://www.npmjs.com/package/axios)  Promise based HTTP client for the browser and node.js
* [commander](https://github.com/tj/commander.js)  for command-line interfaces
* [listr](https://github.com/SamVerschueren/listr)  to create Terminal task list
* [TypeScript](https://www.typescriptlang.org/)
* [Jest](https://flow.org/en) and Enzyme for Unit testing
* [Yarn](https://yarnpkg.com/en/) dependency manager


 ####The command-line tool with the following functions - 

 #### 1. Word Definitions 
 Display definitions of a given word.
 ```shell script
  ./dict defn <word>
```

 #### 2. Word Synonyms
 Display synonyms of a given word. 
 ```shell script
  ./dict syn <word>
 ```

 #### 3. Word Antonyms
 Display antonyms of a given word. Note that not all words would have Antonyms (End point: /relatedWords). Example words with antonyms: single, break, start.
 ```shell script
  ./dic ant <word>
```

 #### 4. Word Examples
 Display examples of usage of a given word in a sentence.
 ```shell script
  ./dict ex <word>
```
 #### 5. Word Full Dict
 Display Word Definitions, Word Synonyms, Word Antonyms & Word Examples for a given word.
 ```shell script
  ./dict <word>
```

 #### 6. Word of the Day Full Dict
 Display Word Definitions, Word Synonyms, Word Antonyms & Word Examples for a random word.
 ```shell script
  ./dict
```

 #### 7. Word Game
 The command should display a definition, a synonym or an antonym and ask the user to guess the word.
 ```shell script
  ./dict play
```
 
Rules:

- If the correct word is entered, show success message
- Any synonyms of the word(expected answer) should be also be accepted as a correct answer.
- If incorrect word is entered, user should be given 3 choices:
    - (1) Try again
        Let the user try again.
    - (2) Hint
        - Display a hint, and let the user try again. Hints could be:
            1. Display the word randomly jumbled (cat => atc, tac, tca)
            2. Display another definition of the word
            3. Display another antonym of the word
            4. Display another synonym of the word
    - (3) Quit
        - Display the Word, Word Full Dict , and quit.

Installation
----

#### Prerequisite

* You need to have [Node.js](https://nodejs.org/en/download/) (> 8) installed.  
* [Yarn](https://yarnpkg.com/lang/en/docs/install/) for dependency management.

Usage
-----

#### How to build? ###
```sh
Steps:
$ yarn install # if not done
$ yarn build
$ cd bin // to execute all the above commands
```

#### How to run test suit? ###
```sh
Steps:
 $ yarn install # if not done
 $ yarn test
```

It should print something like this:-
 
```
Test Suites: 8 passed, 8 total
Tests:       13 passed, 13 total
Snapshots:   0 total
Time:        8.976s
Ran all test suites related to changed files.
```