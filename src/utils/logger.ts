import chalk from "chalk";

export interface LogFormatData {
    label: string;
    results: Array<string>;
}

export const error = chalk.bold.red;
export const warning = chalk.bold.yellow;
export const header = chalk.bold.magenta;
export const label = chalk.bold.blue;
export const text = chalk.green;
export const log = console.log;


export function logResult(data: Array<LogFormatData>): void {

    if (Array.isArray(data)) {
        data.forEach((item) => {
            log(label(`   ${item.label}`));
            if (Array.isArray(item.results)) {
                item.results.forEach((result) => {
                    log(text(`   . ${result}`));
                })
            } else {
                log(warning(`   No word(s) found`));
            }
            log("\n");
        });
    }
}