import {LogFormatData} from "./logger";

export type OnTaskDoneFn = (data: Array<LogFormatData>) => void;

export function getJumbledWord(word: string) {
    const a = word.split(""),
        n = a.length;

    for (let i = n - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        const tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    return a.join("");
}

export function randomNumber(upperLimit: number) {
    return Math.floor(Math.random() * upperLimit);
}