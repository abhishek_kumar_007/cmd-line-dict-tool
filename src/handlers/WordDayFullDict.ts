import FullDict from "./FullDict";
import {error, log} from "../utils/logger";
import FortyTwoWordsCommunicator from "../communicator/FortyTwoWords";

const fortyTwoWordsCommunicator = new FortyTwoWordsCommunicator();

class WordDayFullDict {
    static async runRandomWordFullDictAction() {
        try {
            const randomWord = await fortyTwoWordsCommunicator.getRandomWord();
            FullDict.runFullDictAction(randomWord.word);
        } catch (e) {
            log(error("Error while fetching word of the day full dict", e.message));
        }
    }
}

export default WordDayFullDict;