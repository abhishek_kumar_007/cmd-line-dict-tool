import axios from "axios";
import Examples from "./Examples";

jest.mock("axios");

describe("FortyTwoWordAPI test", () => {

    it("Checking for word examples", async () => {

        const data = {
            examples: [{
                text: "StartExample1"
            }]
        };

        // @ts-ignore
        axios.get.mockImplementation(() => Promise.resolve({data}));

        const response = await Examples.getWordExamples("start");
        expect(response).toEqual(["StartExample1"]);
    })
});