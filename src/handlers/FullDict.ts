import Listr from "listr";
import Examples from "./Examples";
import Definitions from "./Definitions";
import RelatedWords from "./RelatedWords";
import {error, log, LogFormatData, logResult} from "../utils/logger";

export interface FullDictResult {
    definitions: Array<string>;
    synonyms: Array<string>;
    antonyms: Array<string>;
    examples: Array<string>;
}

class FullDict {

    static async getFullDict(word: string): Promise<FullDictResult> {
        try {
            const results: FullDictResult = {
                definitions: [],
                synonyms: [],
                antonyms: [],
                examples: []
            };

            const definitionPromise = Definitions.getWordDefinitions(word);
            const synonymsAntonymsPromise = RelatedWords.getWordSynonymsAntonyms(word);
            const examplesPromise = Examples.getWordExamples(word);

            const definition = await definitionPromise;
            const synonymsAntonyms = await synonymsAntonymsPromise;
            const examples = await examplesPromise;

            if (definition) {
                results.definitions = definition;
            }

            if (synonymsAntonyms.synonyms) {
                results.synonyms = synonymsAntonyms.synonyms;
            }

            if (synonymsAntonyms.antonyms) {
                results.antonyms = synonymsAntonyms.antonyms;
            }

            if (examples) {
                results.examples = examples
            }

            return results;
        } catch (e) {
            log(error("Error while fetching full dict -> ", e.message));
        }
    }

    static runFullDictAction(word: string) {
        try {
            let definition: Array<LogFormatData> = [];
            let synonymsAntonyms: Array<LogFormatData> = [];
            let examples: Array<LogFormatData> = [];

            const tasksConfig1 = Definitions.generateTasks(word, (data) => definition = data);
            const tasksConfig2 = RelatedWords.generateSynonymsAntonymsTasks(
                word,
                data => synonymsAntonyms = data
            );
            const tasksConfig3 = Examples.generateTasks(word, data => examples = data);

            const tasksConfig = tasksConfig1.concat(tasksConfig2).concat(tasksConfig3);

            const tasks = new Listr(tasksConfig, {concurrent: true, exitOnError: false});

            tasks.run()
                .then(() => {
                    logResult(definition);
                    logResult(synonymsAntonyms);
                    logResult(examples);
                })
                .catch(err => {
                    // error
                });

        } catch (e) {
            log(error("Error while fetching full dict -> ", e.message));
        }
    }
}

export default FullDict;