import inquirer from "inquirer";
import FullDict, {FullDictResult} from "./FullDict";
import {getJumbledWord, randomNumber} from "../utils/utils";
import FortyTwoWordsCommunicator from "../communicator/FortyTwoWords";
import {error, header, label, log, logResult, text} from "../utils/logger";

type QuestionWordType = "definitions" | "synonyms" | "antonyms";

interface RandomSelectedWord {
    type: QuestionWordType;
    word: string | null | undefined;
}

const fortyTwoWordsCommunicator = new FortyTwoWordsCommunicator();

const GameSteps = {
    STEP_1_WORD_INPUT: 1,
    STEP_2_INCORRECT_INPUT: 2,
    STEP_3_CORRECT_INPUT: 3,
    STEP_4_QUIT: -1
};

class WordPlay {
    static randomSelectedWords: Array<string> = [];

    static getRandomSelectedWord(response: FullDictResult): RandomSelectedWord {

        const randomQuestionTypes: Array<QuestionWordType> = ["definitions", "synonyms", "antonyms"];
        const randomQuestionType: QuestionWordType = randomQuestionTypes[randomNumber(randomQuestionTypes.length)];
        const word = response[randomQuestionType][randomNumber(response.definitions.length)];

        return {
            type: randomQuestionType,
            word
        };
    }

    static getHintWord(
        selectedWordForQuestion: RandomSelectedWord,
        response: FullDictResult,
        randomWord: string
    ): RandomSelectedWord {
        let selectedWord = {...selectedWordForQuestion};

        while (WordPlay.randomSelectedWords.includes(selectedWord.word)) {
            selectedWord = WordPlay.getRandomSelectedWord(response);

            if (!selectedWord.word || WordPlay.randomSelectedWords.includes(selectedWord.word)) {
                selectedWord.word = getJumbledWord(randomWord);
            }
        }

        return selectedWord;
    }

    static async runWordPlayAction() {
        try {
            const randomWord = await fortyTwoWordsCommunicator.getRandomWord();
            const response: FullDictResult = await FullDict.getFullDict(randomWord.word);

            let selectedWordForQuestion: RandomSelectedWord = {
                type: "definitions",
                word: null
            };

            while (!selectedWordForQuestion.word) {
                selectedWordForQuestion = WordPlay.getRandomSelectedWord(response);
            }

            WordPlay.randomSelectedWords.push(selectedWordForQuestion.word);

            let question = "";

            if (selectedWordForQuestion.type === "definitions") {
                question = `Definition :- ${selectedWordForQuestion.word}`;
            } else if (selectedWordForQuestion.type === "synonyms") {
                question = `Synonym :- ${selectedWordForQuestion.word}`;
            } else if (selectedWordForQuestion.type === "antonyms") {
                question = `Antonym :- ${selectedWordForQuestion.word}`;
            }

            log(header("Welcome to the word play game\n"));
            log(label("Rule - You need to guess the word for given definition, a synonym or an antonym"));
            log(text(question));

            let questionIndex = 1;

            while (questionIndex !== GameSteps.STEP_4_QUIT) {

                if (questionIndex === GameSteps.STEP_1_WORD_INPUT) {
                    const answer = await inquirer.prompt([{
                        type: "input",
                        name: "answer",
                        message: "Guess the word?"
                    }]);

                    questionIndex = answer.answer === randomWord.word ||
                    (answer.answer !== selectedWordForQuestion.word &&
                        !WordPlay.randomSelectedWords.includes(answer.answer) &&
                        response.synonyms.includes(answer.answer)) ?
                        GameSteps.STEP_3_CORRECT_INPUT :
                        GameSteps.STEP_2_INCORRECT_INPUT;

                } else if (questionIndex === GameSteps.STEP_2_INCORRECT_INPUT) {

                    const answer = await inquirer.prompt([{
                        type: "list",
                        name: "hint",
                        message: "Wrong answer :(",
                        choices: ["Try again?", "Hint", "Quit"]
                    }]);

                    if (answer.hint === "Try again?") {
                        questionIndex = GameSteps.STEP_1_WORD_INPUT;
                    } else if (answer.hint === "Hint") {
                        const selectedWord = WordPlay.getHintWord(
                            selectedWordForQuestion,
                            response,
                            randomWord.word
                        );

                        WordPlay.randomSelectedWords.push(selectedWord.word);
                        log(text(`Hint :- ${selectedWord.word}`));

                        questionIndex = GameSteps.STEP_1_WORD_INPUT;
                    } else if (answer.hint === "Quit") {

                        log(text(`Full dict for the word "${randomWord.word}"\n`));

                        logResult([{
                            label: `Definitions for ${randomWord.word} : `,
                            results: response.definitions
                        }]);

                        logResult([{
                            label: `Synonyms for ${randomWord.word} : `,
                            results: response.synonyms
                        }]);

                        logResult([{
                            label: `Antonyms for ${randomWord.word} : `,
                            results: response.antonyms
                        }]);

                        logResult([{
                            label: `Examples for ${randomWord.word} : `,
                            results: response.examples
                        }]);

                        questionIndex = GameSteps.STEP_4_QUIT;
                    }
                } else if (questionIndex === GameSteps.STEP_3_CORRECT_INPUT) {
                    log(label("\nWell done! Correct answer\n"));
                    questionIndex = GameSteps.STEP_4_QUIT;
                }
            }
        } catch (e) {
            log(error("Something went wrong! ", e.message));
        }
    }
}

export default WordPlay;