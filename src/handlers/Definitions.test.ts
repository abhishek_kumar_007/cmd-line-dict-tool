import axios from "axios";
import Definitions from "./Definitions";

jest.mock("axios");

describe("FortyTwoWordAPI test", () => {

    it("Checking for word definitions", async () => {

        const data = [{
            text: "StartDefinition1"
        }];

        // @ts-ignore
        axios.get.mockImplementation(() => Promise.resolve({data}));

        const response = await Definitions.getWordDefinitions("start");
        expect(response).toEqual(["StartDefinition1"]);
    })
});