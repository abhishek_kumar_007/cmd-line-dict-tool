import Listr from "listr";
import {OnTaskDoneFn} from "../utils/utils";
import {LogFormatData, logResult} from "../utils/logger";
import FortyTwoWordsCommunicator from "../communicator/FortyTwoWords";

const fortyTwoWordsCommunicator = new FortyTwoWordsCommunicator();

export interface SynonymsAntonymsResult {
    synonyms: Array<string>;
    antonyms: Array<string>;
}

export interface GetWordSynonymsAntonymsOptions {
    excludeSynonyms?: boolean;
    excludeAntonyms?: boolean;
}

const GetWordSynonymsAntonymsDefaultOptions = {
    excludeSynonyms: false,
    excludeAntonyms: false
};

class RelatedWords {

    static async getWordSynonymsAntonyms(
        word: string,
        options: GetWordSynonymsAntonymsOptions = GetWordSynonymsAntonymsDefaultOptions
    ): Promise<SynonymsAntonymsResult> {
        try {
            let synonyms: Array<string> = [];
            let antonyms: Array<string> = [];
            const response = await fortyTwoWordsCommunicator.getRelatedWords({word});
            if (Array.isArray(response)) {
                response.forEach((item) => {
                    if (item.relationshipType === "antonym" && options.excludeAntonyms !== true) {
                        antonyms = antonyms.concat(item.words);
                    } else if (item.relationshipType === "synonym" && options.excludeSynonyms !== true) {
                        synonyms = synonyms.concat(item.words);
                    }
                });
            }

            return {
                synonyms: options.excludeSynonyms !== true ? synonyms : [],
                antonyms: options.excludeAntonyms !== true ? antonyms : []
            };
        } catch (e) {
            throw new Error("Error while fetching word antonyms and synonyms -> " + e.message);
        }
    }

    static async runWordSynonymsAction(word: string) {
        let response: Array<LogFormatData> = [];
        const tasks = new Listr([
            {
                title: "Fetching Synonyms",
                task: () => {
                    return new Promise((resolve, reject) => {
                        RelatedWords.getWordSynonymsAntonyms(word, {excludeAntonyms: true})
                            .then((data) => {
                                response = [{
                                    label: `Synonyms for ${word} : `,
                                    results: data.synonyms
                                }];
                                resolve();
                            })
                            .catch(reject);
                    });
                }
            }
        ]);

        tasks.run()
            .then(() => {
                logResult(response)
            })
            .catch(err => {
                // error
            });
    }

    static async runWordAntonymsAction(word: string) {
        let response: Array<LogFormatData> = [];
        const tasks = new Listr([
            {
                title: "Fetching Antonyms",
                task: () => {
                    return new Promise((resolve, reject) => {
                        RelatedWords.getWordSynonymsAntonyms(word, {excludeSynonyms: true})
                            .then((data) => {
                                response = [{
                                    label: `Antonyms for ${word} : `,
                                    results: data.antonyms
                                }];
                                resolve();
                            })
                            .catch(reject);
                    });
                }
            }
        ]);

        tasks.run()
            .then(() => {
                logResult(response)
            })
            .catch(err => {
                // error
            });
    }


    static generateSynonymsAntonymsTasks(word: string, onTaskDone: OnTaskDoneFn) {
        return [
            {
                title: "Fetching Synonyms & Antonyms",
                task: () => {
                    return new Promise((resolve, reject) => {
                        RelatedWords.getWordSynonymsAntonyms(word)
                            .then((data) => {
                                onTaskDone([
                                    {
                                        label: `Synonyms for ${word} : `,
                                        results: data.synonyms
                                    },
                                    {
                                        label: `Antonyms for ${word} : `,
                                        results: data.antonyms
                                    }
                                ]);
                                resolve();
                            })
                            .catch(reject);
                    });
                },
            }
        ];
    }

    static runWordSynonymsAntonymsAction(word: string) {
        let response: Array<LogFormatData> = [];
        const tasks = new Listr(
            RelatedWords.generateSynonymsAntonymsTasks(word, (data => response = data))
        );

        tasks.run()
            .then(() => {
                logResult(response)
            })
            .catch(err => {
                // error
            });
    }
}

export default RelatedWords;