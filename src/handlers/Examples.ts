import Listr from "listr";
import {OnTaskDoneFn} from "../utils/utils";
import {LogFormatData, logResult} from "../utils/logger";
import FortyTwoWordsCommunicator from "../communicator/FortyTwoWords";

const fortyTwoWordsCommunicator = new FortyTwoWordsCommunicator();

class Examples {
    static async getWordExamples(word: string): Promise<Array<string>> {

        try {
            let examples: Array<string> = [];
            const response = await fortyTwoWordsCommunicator.getWordExamples({word});
            if (response && response.examples && Array.isArray(response.examples)) {
                examples = response.examples.map(item => item.text);
            }

            return examples;
        } catch (e) {
            throw new Error("Error while fetching word examples ->" + e.message);
        }
    }

    static generateTasks(word: string, onTaskDone: OnTaskDoneFn) {
        return [
            {
                title: "Fetching Examples",
                task: () => {
                    return new Promise((resolve, reject) => {
                        Examples.getWordExamples(word)
                            .then((data) => {
                                onTaskDone([{
                                    label: `Examples for ${word} : `,
                                    results: data
                                }]);
                                resolve();
                            })
                            .catch(reject);
                    });
                }
            }
        ];
    }

    static runWordExamplesAction(word: string) {
        let response: Array<LogFormatData> = [];
        const tasks = new Listr(Examples.generateTasks(word, data => response = data));

        tasks.run()
            .then(() => {
                logResult(response)
            })
            .catch(err => {
                // error
            });
    }
}

export default Examples;