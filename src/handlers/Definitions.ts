import Listr from "listr";
import {OnTaskDoneFn} from "../utils/utils";
import {LogFormatData, logResult} from "../utils/logger";
import FortyTwoWordsCommunicator from "../communicator/FortyTwoWords";

const fortyTwoWordsCommunicator = new FortyTwoWordsCommunicator();

class Definitions {
    static async getWordDefinitions(word: string): Promise<Array<string>> {
        try {
            let definitions: Array<string> = [];
            const response = await fortyTwoWordsCommunicator.getWordDefinition({word});
            if (Array.isArray(response)) {
                definitions = response.map(item => item.text);
            }

            return definitions;
        } catch (e) {
            throw new Error("Error while fetching word definition -> " + e.message);
        }
    }

    static generateTasks(word: string, onTaskDone: OnTaskDoneFn) {
        return [
            {
                title: "Fetching Definitions",
                task: () => {
                    return new Promise((resolve, reject) => {
                        Definitions.getWordDefinitions(word)
                            .then((data) => {
                                onTaskDone([{
                                    label: `Definitions for ${word} : `,
                                    results: data
                                }]);
                                resolve();
                            })
                            .catch(reject);
                    });
                }
            }
        ]
    }

    static runWordDefinitionAction(word: string) {
        let response: Array<LogFormatData> = [];
        const tasks = new Listr(Definitions.generateTasks(word, data => response = data));

        tasks.run()
            .then(() => {
                logResult(response)
            })
            .catch(err => {
                // error
            });
    }
}

export default Definitions;