import axios from "axios";
import RelatedWords from "./RelatedWords";

jest.mock("axios");

describe("FortyTwoWordAPI test", () => {

    it("Checking for synonyms", async () => {

        const data = [
            {
                relationshipType: "antonym",
                words: ["antonym1", "antonym2"]
            },
            {
                relationshipType: "synonym",
                words: ["synonym1", "synonym2"]
            }
        ];

        // @ts-ignore
        axios.get.mockImplementation(() => Promise.resolve({data}));

        const response = await RelatedWords.getWordSynonymsAntonyms("start", {excludeAntonyms: true});
        expect(response).toEqual({
            synonyms: ["synonym1", "synonym2"],
            antonyms: []
        });
    });

    it("Checking for antonyms", async () => {

        const data = [
            {
                relationshipType: "antonym",
                words: ["antonym1", "antonym2"]
            },
            {
                relationshipType: "synonym",
                words: ["synonym1", "synonym2"]
            }
        ];

        // @ts-ignore
        axios.get.mockImplementation(() => Promise.resolve({data}));

        const response = await RelatedWords.getWordSynonymsAntonyms("start", {excludeSynonyms: true});
        expect(response).toEqual({
            synonyms: [],
            antonyms: ["antonym1", "antonym2"]
        });
    })
});