import axios from "axios";
import FortyTwoWordsCommunicator from "./FortyTwoWords";

jest.mock("axios");

const fortyTwoWordsCommunicator = new FortyTwoWordsCommunicator();

describe("FortyTwoWordAPI test", () => {

    it("Checking for FortyTwoWord Api", async () => {

        const data = [{
            text: "Definition"
        }];

        // @ts-ignore
        axios.get.mockImplementation(() => Promise.resolve({data}));

        const response = await fortyTwoWordsCommunicator.getWordDefinition({word: "start"});
        expect(response).toEqual(data);
    })
});