/**
 * FortyTwoWords Communicator to fetch currency exchange value.
 */
import BaseCommunicator, {BaseURL} from "./BaseCommunicator";

const url: BaseURL = process.env.FORTY_TWO_WORDS_BASE_URL;
const accessKey = process.env.FORTY_TWO_WORDS_ACCESS_KEY;

export interface FortyTwoWordsRequestOptions {
    word: string;
}

export interface Definition {
    text: string;
}

export interface RelatedWord {
    relationshipType: "antonym" | "synonym";
    words: Array<string>;
}

export interface Example {
    text: string;
}

export interface ExampleResponse {
    examples: Array<Example>;
}

export interface RandomWord {
    id: string;
    word: string;
}

class FortyTwoWords extends BaseCommunicator {
    constructor() {
        super(url);
    }

    getWordDefinition = async (
        {word}: FortyTwoWordsRequestOptions
    ): Promise<Array<Definition> | null> => {

        const params = {
            "api_key": accessKey
        };

        const response = await this.get(`/word/${word}/definitions`, params);
        if (response && response.data && response.data.error) {
            return null;
        }

        return response.data;
    };

    getRelatedWords = async (
        {word}: FortyTwoWordsRequestOptions
    ): Promise<Array<RelatedWord> | null> => {

        const params = {
            "api_key": accessKey
        };

        const response = await this.get(`/word/${word}/relatedWords`, params);
        if (response && response.data && response.data.error) {
            return null;
        }

        return response.data;
    };

    getWordExamples = async (
        {word}: FortyTwoWordsRequestOptions
    ): Promise<ExampleResponse | null> => {

        const params = {
            "api_key": accessKey
        };

        const response = await this.get(`/word/${word}/examples`, params);
        if (response && response.data && response.data.error) {
            return null;
        }

        return response.data;
    };

    getRandomWord = async (): Promise<RandomWord | null> => {

        const params = {
            "api_key": accessKey
        };

        const response = await this.get(`/words/randomWord`, params);
        if (response && response.data && response.data.error) {
            return null;
        }

        return response.data;
    };
}

export default FortyTwoWords;