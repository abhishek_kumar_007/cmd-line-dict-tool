/**
 * BaseCommunicator class which supports GET Http methods.
 */

import {URL} from "url";
import axios from "axios";

export type BaseURL = string | null | undefined;

class BaseCommunicator {
    baseURL = "";

    constructor(baseURL: BaseURL) {
        if (baseURL) {
            this.baseURL = baseURL;
        }
    }

    static resolveURL(baseURL: BaseURL, path: string): URL {
        if (path.startsWith('/')) {
            path = path.slice(1);
        }

        if (baseURL) {
            const normalizedBaseURL = baseURL.endsWith('/')
                ? baseURL
                : baseURL.concat('/');
            return new URL(path, normalizedBaseURL);
        } else {
            return new URL(path);
        }
    }

    static urlWithSearchParams(url: URL, params: URLSearchParams | null): string {
        if (params) {
            url.search = params.toString();
        }
        return url.toJSON();
    }

    /**
     * Get Http method
     *
     * @param path
     * @param params
     */
    protected async get<TResult = any>(
        path: string,
        params: Record<string, any> | null
    ): Promise<TResult> {

        const url = BaseCommunicator.resolveURL(this.baseURL, path);

        return axios.get(
            url.toJSON(),
            {
                params
            }
        );
    }
}

export default BaseCommunicator;